package bmiCalculator;

public interface BmiCalculator {
	Bmi calculateBmi(Height height, Weight weight);
}
