package bmiCalculator;

public class BmiCalculatorImpl implements BmiCalculator {

	private static final double POUND_TO_KG = 0.45359237;
	private static final double DKG_TO_KG = 0.01;
	private static final double G_TO_KG = 0.001;
	private static final double INCH_TO_M = 0.0254;
	private static final double MM_TO_M = 0.001;
	private static final double DM_TO_M = 0.1;
	private static final double CM_TO_M = 0.01;
	
	/**
	 * Converts the value of the parameter to meters and returns it.
	 * @param height
	 * @return converted height in meters
	 */
	private double convertHeightUnit(Height height){
		switch (height.getUnit()) {
		case MM:
			return height.getValue() * MM_TO_M;
		case CM:
			return height.getValue() * CM_TO_M;
		case DM:
			return height.getValue() * DM_TO_M;
		case M:
			return height.getValue();
		case INCH:
			return height.getValue() * INCH_TO_M;
		default:
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Converts the value of the parameter to kilograms and returns it.
	 * @param weight
	 * @return converted weight in kilograms
	 */
	private double convertWeightUnit(Weight weight){
		switch (weight.getUnit()) {
		case POUND:
			return weight.getValue() * POUND_TO_KG;
		case DKG:
			return weight.getValue() * DKG_TO_KG;
		case G:
			return weight.getValue() * G_TO_KG;
		case KG:
			return weight.getValue();
		default:
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Calculates the BMI index, rounds it to 2 decimal places then categorizes by the index,
	 * after that it creates a new Bmi object with the calculated index and category then returns that object.
	 */
	public Bmi calculateBmi(Height height, Weight weight){
		double convertedHeight = convertHeightUnit(height);
		double convertedWeight = convertWeightUnit(weight);
		double index = Math.round((convertedWeight / Math.pow(convertedHeight, 2)) * 100.0) / 100.0;
		String category;
		if(index < 16){
			category = "Severe Thinness";
		}else if(index < 17){
			category = "Moderate Thinness";
		}else if(index < 18.5){
			category = "Mild Thinness";
		}else if(index < 25){
			category = "Normal";
		}else if(index < 30){
			category = "Overweight";
		}else if(index < 35){
			category = "Obese Class I";
		}else if(index < 40){
			category = "Obese Class II";
		}else{
			category = "Obese Class III";
		}
		return new Bmi(category,index);
	}

}
