package bmiCalculator;

import units.HeightUnit;

public class Height {
	private double value;
	private HeightUnit unit;

	public Height(double value, HeightUnit unit) {
		this.setValue(value);
		this.setUnit(unit);
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		if (value < 0) {
			throw new IllegalArgumentException();
		}
		this.value = value;
	}

	public HeightUnit getUnit() {
		return unit;
	}

	public void setUnit(HeightUnit unit) {
		if (unit == null) {
			throw new IllegalArgumentException();
		}
		this.unit = unit;
	}
}
