package bmiCalculator;

public class Bmi {
	private String category;
	private double index;
	
	public Bmi(String category, double index){
		this.category = category;
		this.index = index;
	}

	public String getCategory() {
		return category;
	}

	public double getIndex() {
		return index;
	}
}
