package bmiCalculator;

import units.WeightUnit;

public class Weight {
	private double value;
	private WeightUnit unit;
	
	public Weight(double value, WeightUnit unit){
		this.value = value;
		this.unit = unit;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		if(value < 0){
			throw new IllegalArgumentException();
		}
	}

	public WeightUnit getUnit() {
		return unit;
	}

	public void setUnit(WeightUnit unit) {
		if(unit == null){
			throw new IllegalArgumentException();
		}
	}
	
	
	
}
