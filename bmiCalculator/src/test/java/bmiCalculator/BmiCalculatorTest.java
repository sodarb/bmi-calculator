package bmiCalculator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import units.HeightUnit;
import units.WeightUnit;

public class BmiCalculatorTest {
	
	private BmiCalculator bmiCalculator;
	private Height height;
	private Weight weight;
	private Bmi bmi;
	
	private void givenAWeightAndHeightWithInitialValuesAndUnits(double heightValue, HeightUnit heightUnit, double weightValue, WeightUnit weightUnit){
		height = new Height(heightValue,heightUnit);
		weight = new Weight(weightValue,weightUnit);
		bmiCalculator = new BmiCalculatorImpl();
	}
	
	private void whenCalculated(){
		bmi = bmiCalculator.calculateBmi(height, weight);
	}
	
	private void thenTheCalculatedBmiIndexAndCategoryIs(double index, String category){
		double calculatedIndex = bmi.getIndex();
		String calculatedCategory = bmi.getCategory();
		assertThat(calculatedIndex, is(index));
		assertThat(calculatedCategory, is(category));
	}

	@Test
	public void testBmiCalculatedCorrectly1() {
		givenAWeightAndHeightWithInitialValuesAndUnits(180,HeightUnit.CM,60,WeightUnit.KG);
		whenCalculated();
		thenTheCalculatedBmiIndexAndCategoryIs(18.52,"Normal");
	}
	
	@Test
	public void testBmiCalculatedCorrectly2(){
		givenAWeightAndHeightWithInitialValuesAndUnits(70,HeightUnit.INCH,200,WeightUnit.POUND);
		whenCalculated();
		thenTheCalculatedBmiIndexAndCategoryIs(28.70,"Overweight");
	}

}
